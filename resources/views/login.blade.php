<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login Form</title>
    <link rel="stylesheet" href="{{asset('css/login-style.css')}}">
</head>

<body>
<body>
<div class="login">
    <div class="login-screen">
        <div class="app-title">
            <img src="{{asset('img/logo-bank-ntt.png')}}">
            <h4>Sistem Kuisioner Kepuasan Pelanggan</h4>
        </div>
        <div class="login-form">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                    <div class="col-md-6">
                        <input id="username" type="text" class="form-control login-field" name="username" placeholder="Username" value="{{ old('username') }}" required autofocus>
                        @if ($errors->has('username'))
                        <br><span class="help-block"> <strong>{{ $errors->first('username') }}</strong> </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control login-field" name="password" placeholder="Password" required>
                        @if ($errors->has('password'))
                            <span class="help-block"> <strong>{{ $errors->first('password') }}</strong> </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-primary btn-block">
                            Login
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>


</body>
</html>
