<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!--CSRF-->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- jQuery 2.2.3-->
    <script language="JavaScript" type="text/javascript" src="{{ asset('js/plugins/jquery-2.2.3.min.js') }}"></script>
    <script language="JavaScript" type="text/javascript" src="{{ asset('js/plugins/bootstrap.min.js') }}"></script>

    <!-- Globalize scripts -->
    <script type="text/javascript" src="{{ asset('js/plugins/cldr.min.js') }} "></script>
    <script type="text/javascript" src="{{ asset('js/plugins/event.min.js') }} "></script>
    <script type="text/javascript" src="{{ asset('js/plugins/supplemental.min.js') }} "></script>
    <script type="text/javascript" src="{{ asset('js/plugins/globalize.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/message.js') }} "></script>
    <script type="text/javascript" src="{{ asset('js/plugins/number.js') }} "></script>
    <script type="text/javascript" src="{{ asset('js/plugins/currency.js') }} "></script>
    <script type="text/javascript" src="{{ asset('js/plugins/date.js') }}"></script>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/devextreme/dx.common.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/devextreme/dx.light.css') }}"/>

    <!-- A DevExtreme library -->
    <script type="text/javascript" src="{{ asset('js/plugins/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/dx.viz-web.js') }}"></script>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <!-- Theme style -->
    <link href="{{ asset('css/AdminLTE.min.css') }}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/skins/_all-skins.min.css') }}" media="all" rel="stylesheet" type="text/css"/>

    <!-- AdminLTE App -->
    <script type="text/javascript" src="{{ asset('js/apptemplate.min.js') }}"></script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header"> @include('layouts.header')</header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar"> @include('layouts.sidebar')</aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="display: flex; flex-flow: column; height: 100%;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> @yield('title') </h1>
        </section>

        <!-- Main content -->
        <section class="content" style="flex: 1 1 auto; width: 100%  ; display: flex;flex-flow: column; max-height: 100%;"> @yield('content')</section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer"> @include('layouts.footer')</footer>

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


</body>
</html>