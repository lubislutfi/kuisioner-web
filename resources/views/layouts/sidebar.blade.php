<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="{{ asset('img/avatar.png') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p>Selamat datang,<br> {{ Auth::user()->username }}</p>
        </div>
    </div>

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
            <a href="{{ url('/') }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right">
                    </i>
                </span>
            </a>
        </li>

        <li class="treeview {{ Request::is('data-cabang') || Request::is('data-petugas') || Request::is('data-vote') ? 'active' : '' }}">
            <a href="#">
                <i class="fa fa-database"></i> <span>Tampilkan Data</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ Request::is('data-cabang') ? 'active' : '' }}"><a href="{{ url('/data-cabang') }}"><i
                            class="fa fa-building-o"></i> Pivot Table</a></li>
                <li class="{{ Request::is('data-vote') ? 'active' : '' }}"><a href="{{ url('/data-vote') }}"><i
                            class="fa fa-meh-o"></i>Live Stream</a></li>
                <!--   <li class="{{ Request::is('data-vote') ? 'active' : '' }}"><a href="{{ url('/data-vote') }}"><i class="fa fa-thumbs-o-up"></i> Berdasarkan Pertanyaan</a></li>
               --></ul>
        </li>

        <li class="treeview {{ Request::is('pengaturan-cabang') || Request::is('pengaturan-user') || Request::is('pengaturan-pertanyaan') ? 'active' : '' }}">
            <a href="#">
                <i class="fa fa-cog"></i> <span>Pengaturan</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ Request::is('pengaturan-pertanyaan') ? 'active' : '' }}"><a
                        href="{{ url('/pengaturan-pertanyaan') }}"><i class="fa fa-pencil-square-o"></i> Daftar
                        Pertanyaan</a></li>
                <li class="{{ Request::is('pengaturan-cabang') ? 'active' : '' }}"><a
                        href="{{ url('/pengaturan-cabang') }}"><i class="fa fa-building-o"></i> Daftar Cabang</a></li>
                <li class="{{ Request::is('pengaturan-user') ? 'active' : '' }}"><a
                        href="{{ url('/pengaturan-user') }}"><i class="fa fa-users"></i> Manajemen User</a></li>
            </ul>
        </li>
    </ul>
</section>
<!-- /.sidebar -->