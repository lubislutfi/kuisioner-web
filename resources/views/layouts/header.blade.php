<!-- Logo -->
<a href="{{ url('/') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>NTT</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Bank</b>NTT</span>
</a>

<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="dropdown messages-menu">
                <a href="{{ url('/keluar') }}" class=" dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-sign-out"> </i><span> Logout</span>
                </a>
                <form id="logout-form" action="{{ url('/keluar') }}" method="GET" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </div>
</nav>