@extends('layouts.default')
@section('title', 'Pengaturan User')
@section('content')

<div class="panel panel-default">

    <div class="panel-body">
        <div id="gridContainer" style="width: 100%; height: 100%"></div>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-md-2">
                 <a href="{{ url('/pengaturan-user/create') }}" class="btn btn-primary" role="button">Tambah User</a>
             </div>
            <div class="col-md-8">
            </div>
            <div class="col-md-1">
                <a id="btnHapus" class="btn btn-block btn-danger pull-right jquery-postback" role="button">Hapus</a>
            </div>
            <div class="col-md-1">
                <a id="btnEdit" class="btn btn-block btn-info pull-left" role="button">Edit</a>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var selectedId;

        var cabangStore = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred(), args = {};
                if (loadOptions.sort) {
                    args.orderby = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc) {
                        args.orderType += "desc";
                    }
                }

                //args.skip = loadOptions.skip || 0;
                //args.take = loadOptions.take || 12;
                //args.filterOptions = loadOptions.filter ? JSON.stringify(loadOptions.filter) : "";
                args.page = (loadOptions.skip + loadOptions.take) / loadOptions.take;
                var x = location.origin;

                $.ajax({
                    url:"json-user",
                    data: args,
                    success: function (result) {
                        deferred.resolve(result.data, {totalCount: result.total});
                        //console.log(result);
                    },
                    error: function () {
                        deferred.reject("Data Loading Error");
                    },
                    timeout: 7000
                });
                //console.log(args);
                //console.log(loadOptions);
                return deferred.promise();
            },
            update: function (key, values) {
                console.log("UPDATE");
                console.log(key);
                console.log(values);
            },
            remove: function (key) {
                console.log("REMOVE");
                console.log(key);
            },
        });

        var grid = $("#gridContainer").dxDataGrid({
            height: "100%",
            dataSource: {
                store: cabangStore
            },
            selection: {
                mode: "single"
            },
            hoverStateEnabled: true,
            paging: {
                pageSize: 15
            },
            columns: [
                {
                    dataField: 'name',
                    caption: 'Nama Lengkap'
                }, {
                    dataField: 'username',
                    caption: 'Username'
                }, {
                    dataField: 'email',
                    caption: 'E-Mail'
                }, {
                    dataField: 'usergroup.name',
                    caption: 'Jenis User',
                    alignment: 'left'
                }, {
                    dataField: 'branches.name',
                    caption: 'Cabang'
                }
            ],
            onSelectionChanged: function (selectedItems) {
                selectedId = selectedItems.selectedRowsData[0].id;
                if (selectedId) {
                    $('#btnEdit').attr('href','pengaturan-user/'+selectedId+'/edit');
                }
            }
        }).dxDataGrid("instance");

        $("#btnEdit").click(function() {
            if(!selectedId)
            {
                DevExpress.ui.notify('Pilih user terlebih dahulu', 'warning', 800);
            }
        });

        $("#btnHapus").click(function() {
            if(!selectedId)
            {
                DevExpress.ui.notify('Pilih user terlebih dahulu', 'warning', 800);
            }else {
                $.ajax({
                    url: 'pengaturan-user/'+selectedId,
                    type: 'DELETE',
                    success: function(result) {
                        grid.refresh();
                        DevExpress.ui.notify('Data berhasil dihapus', 'success', 800);
                    }
                });
            }
        });

    });



</script>

@stop