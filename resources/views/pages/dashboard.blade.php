@extends('layouts.default')
@section('title', 'Dashboard')
@section('content')

<!-- Main row -->
<div class="row">
    <div class="col-md-12">
        <!-- USERS LIST -->
        <div class="box">
            <div class="box-header with-border ">
                <h3 class="box-title">Line Chart</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="col-md-6">
                    <div id="line_harian"></div>
                </div>
                <div class="col-md-6">
                    <div id="line_bulanan"></div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!--/.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <!-- USERS LIST -->
        <div class="box box-warning ">
            <div class="box-header with-border">
                <h3 class="box-title">5 Pegawai Terbaik</h3>
                <div class="pull-right box-tools">

                    <div class="dx-field">
                        <div class="dx-field-label">Periode:</div>
                        <div class="dx-field-value">
                            <div id="select-periode"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="col-md-12">
                    <div id="gridBestOverall"></div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!--/.box -->
    </div>
    <!-- /.col -->
</div>

<div class="row">
    <div class="col-md-6">
        <!-- USERS LIST -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">5 Teller Terbaik</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="col-md-12">
                    <div id="gridBestTeller"></div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!--/.box -->
    </div>
    <!-- /.col -->

    <div class="col-md-6">
        <!-- USERS LIST -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">5 Customer Service Terbaik</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="col-md-12">
                    <div id="gridBestCS"></div>
                </div>
                <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
            </div>
            <!-- /.box-body -->
        </div>
        <!--/.box -->
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Data Per Cabang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="col-md-12">
                    <div id="bar_cabang" style="height:800px"></div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!--/.box -->
    </div>
    <!-- /.col -->
</div>
<script>
    $(document).ready(function () {

        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var startBulan = new Date(2016, 12, 0);
        var endBulan = new Date();
        var dataBulan = [];
        var formatter_display = Globalize("en").dateFormatter({skeleton: "yMMM"});
        var selectedPeriode = "0000/0";

        //untuk semua periode
        dataBulan.push({
            "value": "0000"+"/"+"0",
            "display": "Lifetime"
        });

        for (var d = endBulan; d >= startBulan; d.setMonth(d.getMonth() - 1)) {
            var periode = new Date(d);
            dataBulan.push({
                "value": periode.getFullYear()+"/"+(periode.getMonth()+1),
                "display": formatter_display(periode)
            });
        }

        var line_harian = $("#line_harian").dxChart({
            palette: "Ocean",
            commonSeriesSettings: {
                argumentField: "tanggal",
                type: 'line'
            },
            margin: {
                bottom: 20
            },
            argumentAxis: {
                valueMarginsEnabled: false,
                discreteAxisDivisionMode: "crossLabels",
                grid: {
                    visible: true
                }
            },
            valueAxis: [
                {
                    valueType: 'numeric',
                }
            ],
            series: [
                {valueField: "poin", name: "Poin"},
            ],
            legend: {
                verticalAlignment: "bottom",
                horizontalAlignment: "center",
                itemTextPosition: "bottom"
            },
            title: {
                text: "Harian"
            },
            "export": {
                enabled: true
            },
            tooltip: {
                enabled: true,
                customizeTooltip: function (arg) {
                    return {
                        text: arg.argumentText + "\n" + arg.valueText.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") + " Poin"
                    };
                }
            }
        }).dxChart("instance");

        var line_bulanan = $("#line_bulanan").dxChart({
            palette: "violet",
            commonSeriesSettings: {
                argumentField: "bulan",
                type: 'line'
            },
            margin: {
                bottom: 20
            },
            valueAxis: [
                {
                    valueType: 'numeric',
                }
            ],
            argumentAxis: {
                valueMarginsEnabled: false,
                discreteAxisDivisionMode: "crossLabels",
                grid: {
                    visible: true
                }
            },
            series: [
                {valueField: "poin", name: "Poin"},
            ],
            legend: {
                verticalAlignment: "bottom",
                horizontalAlignment: "center",
                itemTextPosition: "bottom"
            },
            title: {
                text: "Bulanan"
            },
            "export": {
                enabled: true
            },
            tooltip: {
                enabled: true,
                customizeTooltip: function (arg) {
                    return {
                        text: arg.argumentText + "\n" + arg.valueText.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") + " Poin"
                    };
                }
            }
        }).dxChart("instance");

        var selectPeriodeBarCabang = $("#select-periode").dxSelectBox({
            dataSource: dataBulan,
            displayExpr: "display",
            valueExpr: "value",
            value: dataBulan[0].value,
            onValueChanged: function (data) {
                console.log(data.value);
                selectedPeriode = data.value;
                store.load();
            }
        }).dxSelectBox("instance");

        var bar_cabang = $("#bar_cabang").dxChart({
            rotated: true,
            commonSeriesSettings: {
                argumentField: "branch",
                type: "stackedBar"
            },
            series: [
                {valueField: "cs", name: "CS"},
                {valueField: "teller", name: "Teller"}
            ],
            legend: {
                verticalAlignment: "bottom",
                horizontalAlignment: "center",
                itemTextPosition: 'top'
            },
            valueAxis: {
                valueType: 'numeric',
                title: {
                    text: "Poin"
                },
                position: "right"
            },
            "export": {
                enabled: true
            },
            tooltip: {
                enabled: true,
                location: "edge",
                customizeTooltip: function (arg) {
                    return {
                        text: arg.seriesName + "\nPoin: " + arg.valueText
                    };
                }
            }
        }).dxChart("instance");

        var gridBestOverall = $("#gridBestOverall").dxDataGrid({
            //dataSource: customers,
            columns: [
                {
                    dataField: 'name',
                    caption: 'Nama'
                },
                {
                    dataField: 'usergroup',
                    caption: 'Posisi Petugas'
                },
                {
                    dataField: 'branch',
                    caption: 'Cabang'
                },
                {
                    dataField: 'poin',
                    caption: 'Poin'
                }
            ]
        }).dxDataGrid("instance");

        var gridBestTeller = $("#gridBestTeller").dxDataGrid({
            //dataSource: customers,
            columns: [
                {
                    dataField: 'name',
                    caption: 'Nama'
                },
                {
                    dataField: 'branch',
                    caption: 'Cabang'
                },
                {
                    dataField: 'poin',
                    caption: 'Poin'
                }
            ]
        }).dxDataGrid("instance");

        var gridBestCS = $("#gridBestCS").dxDataGrid({
            //dataSource: customers,
            columns: [
                {
                    dataField: 'name',
                    caption: 'Nama'
                },
                {
                    dataField: 'branch',
                    caption: 'Cabang'
                },
                {
                    dataField: 'poin',
                    caption: 'Poin'
                }
            ]
        }).dxDataGrid("instance");

        var store = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var params = {};
                console.log(loadOptions);
                var deferred = $.Deferred();
               /* $.get('json-dashboard', loadOptions).done(function (response) {
                    line_harian.option('dataSource', response.linechart_harian);
                    line_bulanan.option('dataSource', response.linechart_bulanan);
                    gridBestOverall.option('dataSource', response.best_overall);
                    gridBestTeller.option('dataSource', response.best_teller);
                    gridBestCS.option('dataSource', response.best_cs);
                    bar_cabang.option('dataSource', response.barchart_cabang);
                    deferred.resolve(response);
                });*/

               if(selectedPeriode != "0000/0")
               {
                   params.periode = selectedPeriode;
               }

                $.ajax({
                    url: 'json-dashboard',
                    type: 'POST',
                    data: params,
                    async: false,
                    success: function (response) {
                        line_harian.option('dataSource', response.linechart_harian);
                        line_bulanan.option('dataSource', response.linechart_bulanan);
                        gridBestOverall.option('dataSource', response.best_overall);
                        gridBestTeller.option('dataSource', response.best_teller);
                        gridBestCS.option('dataSource', response.best_cs);
                        bar_cabang.option('dataSource', response.barchart_cabang);
                        deferred.resolve(response);
                    }
                });

                return deferred.promise();
            }
        });


        store.load();
    })
    ;
</script>

@stop
