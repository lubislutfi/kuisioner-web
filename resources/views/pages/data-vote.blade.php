@extends('layouts.default')
@section('title', 'Vote Live Stream')
@section('content')
<div class="row"
     style="flex: 1 1 auto;display: flex;flex-flow: column; max-height: 100%; padding-left: 8px; padding-right: 8px">
    <div id="datagrid" style="flex: 1 1 auto;"></div>
</div>

<script>
    $(document).ready(function () {

        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var store = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred(), params = {};

                //Paging options
                if (loadOptions.skip) {
                    params.skip = loadOptions.skip;
                }

                if (loadOptions.take) {
                    params.take = loadOptions.take;
                }

                //Filter
                if (loadOptions.filter) {

                    if (!Array.isArray(loadOptions.filter[0])) {
                        var paymentFilter;

                        paymentFilter = loadOptions.filter;
                        params.filter = paymentFilter;
                    } else {
                        var paymentFilter = [];
                        for (var i = 0; i < loadOptions.filter.length; i++) {
                            if (Array.isArray(loadOptions.filter[i])) {
                                if (Array.isArray(loadOptions.filter[i][0])) {
                                    var paymentFilterChild = [];
                                    for (var j = 0; j < loadOptions.filter[i].length; j++) {
                                        if (Array.isArray(loadOptions.filter[i][j])) {
                                            if (loadOptions.filter[i][j][0] == "waktu") {
                                                var date = loadOptions.filter[i][j][2];
                                                var date_string = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? '' : '0') + (date.getMonth() + 1) + "-" + ((date.getDate()) > 9 ? '' : '0') + (date.getDate());

                                                paymentFilterChild.push([loadOptions.filter[i][j][0], loadOptions.filter[i][j][1], date_string]);
                                            }
                                        } else {
                                            paymentFilterChild.push(loadOptions.filter[i][j]);
                                        }
                                    }
                                    paymentFilter.push(paymentFilterChild);

                                } else if (loadOptions.filter[i][0] == "waktu") {

                                    var date = loadOptions.filter[i][2];
                                    var date_string = date.getFullYear() + "-" + ((date.getMonth() + 1) > 9 ? '' : '0') + (date.getMonth() + 1) + "-" + ((date.getDate()) > 9 ? '' : '0') + (date.getDate());

                                    paymentFilter.push([loadOptions.filter[i][0], loadOptions.filter[i][1], date_string]);

                                } else {
                                    paymentFilter.push(loadOptions.filter[i]);
                                }

                            } else {
                                paymentFilter.push(loadOptions.filter[i]);
                            }
                            params.filter = paymentFilter;

                        }
                    }
                }

                //Getting sort options
                if (loadOptions.sort) {
                    params.orderBy = loadOptions.sort[0].selector;
                    (loadOptions.sort[0].desc) ? params.orderType = 'desc' : params.orderType = 'asc';
                }

                console.log(params);

                $.ajax({
                    url: "json-vote",
                    data: params,
                    method: 'POST',
                    success: function (result) {
                        deferred.resolve(
                            result.data,
                            {
                                totalCount: result.totalCount
                            }
                        );
                    },
                    error: function (jqXHR, exception, errorThrown) {
                        var msg = '';
                        if (jqXHR.status === 0) {
                            msg = 'Not connect.\n Verify Network.';
                        } else if (jqXHR.status == 404) {
                            msg = 'Requested page not found. [404]';
                        } else if (jqXHR.status == 500) {
                            msg = 'Internal Server Error [500].';
                        } else if (exception === 'parsererror') {
                            msg = 'Requested JSON parse failed.';
                        } else if (exception === 'timeout') {
                            msg = 'Time out error.';
                        } else if (exception === 'abort') {
                            msg = 'Ajax request aborted.';
                        } else {
                            msg = 'Uncaught Error.\n' + jqXHR.responseText;
                        }
                        console.log(msg);
                        console.log(exception);
                        console.log(errorThrown);

                        deferred.reject("Data Loading Error");
                    }
                });
                //console.log(args);
                // console.log(loadOptions);
                return deferred.promise();
            }
        });

        var grid = $("#datagrid").dxDataGrid({
            height: '200px',
            dataSource: {
                store: store
            },
            scrolling: {
                mode: 'virtual'
            },
            remoteOperations: {
                paging: true,
                filtering: true,
                sorting: true,
            },
            selection: {
                mode: "single",
            },
            hoverStateEnabled: true,
            filterRow: {
                visible: true
            },
            columnChooser: {
                enabled: true,
                emptyPanelText: 'Geser kolom kesini untuk menyembunyikan'
            },
            "export": {
                enabled: true,
                fileName: "Kuisioner Data",
            },
            paging: {
                enabled: true,
                pageSize: 30
            },
            columns: [
                {
                    dataField: 'vote_id',
                    caption: 'Id',
                    visible: false,
                    sortOrder: 'desc'
                }, {
                    dataField: 'waktu',
                    caption: 'waktu',
                    dataType: "date",
                    format: "shortDateShortTime",
                    allowFiltering: true,
                }, {
                    dataField: 'nama',
                    caption: 'Nama Petugas'
                }, {
                    dataField: 'usergroup',
                    caption: 'Jenis Petugas'
                }, {
                    dataField: 'branch',
                    caption: 'Cabang'
                }, {
                    dataField: 'branchgroup',
                    caption: 'Jenis Cabang',
                    visible: false
                }, {
                    dataField: 'value',
                    width: 70,
                    dataType: "number",
                    caption: 'Poin'
                }, {
                    dataField: 'id_branch',
                    caption: 'Id Cabang',
                    visible: false
                }, {
                    dataField: 'id_usergroup',
                    caption: 'Id Jenis Petugas',
                    visible: false
                }
            ]
        }).dxDataGrid("instance");

        window.setInterval(function () {
            grid.refresh();
        }, 60000);
    });


</script>
@stop