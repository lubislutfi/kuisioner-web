@extends('layouts.default')
@section('title', 'User Baru')
@section('content')
<div class="container">
    <div id="alert_container" class="alert alert-warning alert-dismissable fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    </div>

    <form class="form-horizontal">
        {{ csrf_field() }}
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Masukkan data user</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="form-group{{ $errors->has('id') ? ' has-error' : '' }}" style="display: none;"><!---->
                        <label for="id" class="col-md-3 control-label">ID</label>
                        <div class="col-md-6">
                            <input id="id" type="text" class="form-control" name="id"
                                   value="{{ isset($user) ? $user[0]->id : old('id') }}" hidden>
                            @if ($errors->has('id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-3 control-label">Nama Lengkap</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name"
                                   value="{{ isset($user) ? $user[0]->name : old('name') }}"
                                   required
                                   autofocus>

                            @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
                        <label for="username" class="col-md-3 control-label">Username</label>

                        <div class="col-md-6">
                            <input id="username" type="text" class="form-control" name="username"
                                   value="{{ isset($user) ? $user[0]->username : old('username') }}"
                                   required>
                            @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-3 control-label">E-mail</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email"
                                   value="{{ isset($user) ? $user[0]->email : old('email') }}"
                                   required>

                            @if ($errors->has('email'))
                            <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-3 control-label">Password</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" {{ isset($user) ?
                            '' : 'required' }}>
                            @if ($errors->has('password'))
                            <span class="help-block">
                       <strong>{{ $errors->first('password') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
                        <label for="confirm_password" class="col-md-3 control-label">Confirm Password</label>

                        <div class="col-md-6">
                            <input id="confirm_password" type="password" class="form-control" name="confirm_password" {{
                                   isset($user) ? '' : 'required' }}>
                            @if ($errors->has('password'))
                            <span class="help-block">
                        <strong>{{ $errors->first('confirm_password') }}</strong>
                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('branch_id') ? ' has-error' : '' }}">
                        <label for="branch_id" class="col-md-3 control-label">Cabang</label>
                        <div class="col-md-6">
                            <select id="branch_id" class="form-control" name="branch_id"
                                    value="{{ isset($user) ? $user[0]->branch_id : old('branch_id') }}">
                                @foreach ($branchoptions as $branchoption)
                                <option value="{{ $branchoption->id }}"
                                        @if(isset($user))
                                        @if( $branchoption->id == $user[0]->branch_id)
                                    selected
                                    @endif
                                    @endif >
                                    {{ $branchoption->name }}
                                </option>
                                @endforeach
                            </select>
                            @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('branch_id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('usergroup') ? ' has-error' : '' }}">
                        <label for="usergroup_id" class="col-md-3 control-label">Jenis User</label>
                        <div class="col-md-6">
                            <select id="usergroup_id" class="form-control" name="usergroup_id"
                                    value="{{ isset($user) ? $user[0]->usergroup_id : old('usergroup_id') }}">
                                @foreach ($usergroups as $usergroup)
                                <option value="{{ $usergroup->id }}"
                                        @if(isset($user))
                                        @if( $usergroup->id == $user[0]->usergroup->id)
                                    selected
                                    @endif
                                    @endif >
                                    {{ $usergroup->name }}
                                </option>
                                @endforeach
                            </select>
                            @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('usergroup') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-8">
                        <a href="{{ url('/pengaturan-user') }}" class="btn btn-danger pull-right"
                           role="button">Batal</a>
                    </div>

                    <div class="col-sm-1">
                        <a id="btnSubmit" class="btn btn-primary"
                           role="button">Simpan</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {

        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    //initialize variable
    var url;
    var method;
    var type;
    var data;

    //hiding alert
    $("#alert_container").hide();

    //defining edit or create
    if ($('#username').val() == '') {
        url = '/kuisioner-ntt-web/public/pengaturan-user';
        method = 'POST';
        type = 'create';

        console.log(url);
        console.log(method);
        console.log(type);
    } else {
        url = '/kuisioner-ntt-web/public/pengaturan-user/' + $("#id").val();
        method = 'PATCH';
        type = 'edit';

        console.log(url);
        console.log(method);
        console.log(type);
    }

    $("#btnSubmit").click(function () {

        data = {
            'id': $("#id").val(),
            'name': $("#name").val(),
            'username': $("#username").val(),
            'email': $("#email").val(),
            'password': $("#password").val(),
            'confirm_password': $("#confirm_password").val(),
            'branch_id': $("#branch_id").val(),
            'usergroup_id': $("#usergroup_id").val(),
        };

        /*     $("#name").val()
         $("#username").val()
         $("#email").val()
         $("#password").val()
         $("#confirm_password").val()
         $("#branch_id").val()
         $("#usergroup_id").val()*/
        //console.log('foo');
        //console.log(data);
        //console.log('bar');
        $.ajax({
            url: url,
            type: method,
            data: data,
            success: function (result) {
                
                console.log(result);
                //console.log(result.confirm_password);

                //window.location.replace("/pengaturan-user");
                if(result == 'success'){
                    DevExpress.ui.notify('Berhasil Disimpan', 'success', 600);
                    window.location.replace("/kuisioner-ntt-web/public/pengaturan-user");
                }else
                {
                    $("#alert_container").show();

                    // variable keperluan pembuatan alert
                    var node = document.createTextNode(result[key]);
                    var ul = document.createElement("ul");
                    var element = document.getElementById("alert_container");

                    //clear child
                    while (element.hasChildNodes()) {
                        element.removeChild(element.lastChild);
                    }

                    //window.location.replace("/pengaturan-user");
                    //ambil pesan error
                    for (var key in result) {

                        var li = document.createElement("li");
                        var node = document.createTextNode(result[key]);
                        ul.appendChild(li);
                        li.appendChild(node);
                        element.appendChild(ul);

                       // console.log(' name=' + key + ' value=' + result[key]);
                    }
                }
            }
        });
    });

</script>

@stop