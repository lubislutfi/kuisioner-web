@extends('layouts.default')
@section('title', 'Data Cabang')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div id="range-selector"></div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div id="pivotgrid"></div>
    </div>

</div>
<script>
    $(document).ready(function () {

        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var perPage = 1000;
        var take = perPage;
        var skip = 0;
        var total = 0;
        var pivot_data = [];

        var bulanEnd = new Date();
        var bulanStart = new Date();
        bulanStart.setMonth(bulanStart.getMonth() - 1);



        var rangeSelector = $("#range-selector").dxRangeSelector({
            size: {
                height: 120
            },
            scale: {
                startValue: new Date(2016, 11, 1),
                endValue: bulanEnd,
                minorTickInterval: "month",
                tickInterval: {months: 1},
            },
            sliderMarker: {
                format: "monthAndYear"
            },
            selectedRange: {
                startValue: bulanStart,
                endValue: bulanEnd,
            },
            onSelectedRangeChanged: function (e) {
                bulanStart = e.startValue;
                bulanEnd = e.endValue;
                myPivotGridDataSource.reload();
            }
        }).dxRangeSelector("instance");

        var store = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred();

                $.ajax({
                    url: 'json-pivot-count',
                    type: 'POST',
                    data: {
                        "bulanStart": bulanStart.getFullYear() + "/" + (bulanStart.getMonth()+1),
                        "bulanEnd": bulanEnd.getFullYear() + "/" + (bulanEnd.getMonth()+1)
                    },
                    async: false,
                    success: function (result_count) {
                        var x = 0;
                        perPage = 1000;
                        take = perPage;
                        skip = 0;
                        pivot_data = [];

                        while (skip <= Number(result_count)) {
                            x++;

                            var params = {
                                "take": take,
                                "skip": skip,
                                "bulanStart": bulanStart.getFullYear() + "/" + (bulanStart.getMonth()+1),
                                "bulanEnd": bulanEnd.getFullYear() + "/" + (bulanEnd.getMonth()+1)
                            };

                            $.ajax({
                                url: 'json-pivot',
                                type: 'POST',
                                data: params,
                                async: false,
                                success: function (result) {
                                    pivot_data = pivot_data.concat(result);
                                    skip += result.length;
                                    take = perPage;

                                }
                            });

                            if (skip == Number(result_count)) {
                                break;
                            }

                        }
                        console.log(pivot_data);
                        deferred.resolve(pivot_data);
                    }
                });
                return deferred.promise();
            }
        });

        var myPivotGridDataSource = new DevExpress.data.PivotGridDataSource({
            fields: [{
                caption: "Tanggal",
                dataField: "tanggal",
                dataType: "date",
                area: "column",
            }, {
                caption: "Petugas",
                dataField: "votes.user.name",
                area: "filter"
            }, {
                caption: "Cabang",
                dataField: "votes.user.branches.name",
                area: "row"
            }, {
                caption: "Jenis Petugas",
                dataField: "votes.user.usergroup.name",
                area: "filter"
            }, {
                caption: "Pertanyaan",
                dataField: "questionlist.question",
                area: "filter"
            }, {
                caption: "Poin",
                dataField: "value",
                dataType: "number",
                summaryType: "sum",
                area: "data"
            }, {
                caption: "Jumlah Voter",
                dataField: "value",
                dataType: "number",
                summaryType: "count",
                area: "data"
            }],
            store: store
        });

        var pivotgrid = $("#pivotgrid").dxPivotGrid({
            allowSortingBySummary: true,
            allowSorting: true,
            allowFiltering: true,
            showBorders: true,
            export: {
                enabled: true,
                fileName: "Kuisioner Pivot Grid"
            },
            fieldChooser: {
                enabled: false
            },
            fieldPanel: {
                showColumnFields: true,
                showDataFields: true,
                showFilterFields: true,
                showRowFields: true,
                allowFieldDragging: true,
                visible: true
            },
            dataSource: myPivotGridDataSource,
            height: 570
        }).dxPivotGrid("instance");
    });
</script>
@stop