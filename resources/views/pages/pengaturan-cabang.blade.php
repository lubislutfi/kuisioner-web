@extends('layouts.default')
@section('title', 'Pengaturan Cabang')
@section('content')
<div class="row">
    <div class="panel panel-default" style="margin-right: 8px;margin-left: 8px">
        <div class="panel-body">
            <div id="gridContainer" style="width: 100%; height: 100%"></div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-lg-2">
                    <a href="{{ url('/pengaturan-cabang/create') }}" class="btn btn-primary" role="button">Tambah
                        Cabang</a>
                </div>
                <div class="col-lg-8">
                </div>
                <div class="col-lg-1">
                    <a id="btnHapus" class="btn btn-block btn-danger pull-right jquery-postback" role="button">Hapus</a>
                </div>
                <div class="col-lg-1">
                    <a id="btnEdit" class="btn btn-block btn-info pull-left" role="button">Edit</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {

        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var selectedId;
        var cabangStore = new DevExpress.data.CustomStore({
            load: function (loadOptions) {
                var deferred = $.Deferred(), args = {};
                if (loadOptions.sort) {
                    args.orderby = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc) {
                        args.orderType += "desc";
                    }
                }

                //args.skip = loadOptions.skip || 0;
                //args.take = loadOptions.take || 12;
                //args.filterOptions = loadOptions.filter ? JSON.stringify(loadOptions.filter) : "";
                args.page = (loadOptions.skip + loadOptions.take) / loadOptions.take;
                var x = location.origin;

                $.ajax({
                    url: x + "/kuisioner-ntt-web/public/json-branches",
                    data: args,
                    success: function (result) {
                        deferred.resolve(result.data, {totalCount: result.total});
                        console.log(result);
                    },
                    error: function () {
                        deferred.reject("Data Loading Error");
                    },
                    timeout: 5000
                });
                console.log(args);
                console.log(loadOptions);
                return deferred.promise();
            }
        });

        var grid = $("#gridContainer").dxDataGrid({
            height: "100%",
            dataSource: {
                store: cabangStore
            },
            paging: {
                pageSize: 15
            },
            selection: {
                mode: "single"
            },
            hoverStateEnabled: true,
            columns: [
                {
                    dataField: 'name',
                    caption: 'Nama Cabang'
                },
                {
                    dataField: 'branchgroup.name',
                    caption: 'Keterangan'
                }
            ],
            onSelectionChanged: function (selectedItems) {
                selectedId = selectedItems.selectedRowsData[0].id;
                if (selectedId) {
                    $('#btnEdit').attr('href', '/kuisioner-ntt-web/public/pengaturan-cabang/' + selectedId + '/edit');
                }
            }
        }).dxDataGrid("instance");

        $("#btnEdit").click(function () {
            if (!selectedId) {
                DevExpress.ui.notify('Pilih cabang terlebih dahulu', 'warning', 800);
            }
        });

        $("#btnHapus").click(function () {
            if (!selectedId) {
                DevExpress.ui.notify('Pilih cabang terlebih dahulu', 'warning', 800);
            } else {
                $.ajax({
                    url: '/kuisioner-ntt-web/public/pengaturan-cabang/' + selectedId,
                    type: 'DELETE',
                    success: function (result) {
                        grid.refresh();
                        DevExpress.ui.notify('Data berhasil dihapus', 'success', 800);
                    }
                });
            }
        });


    });
</script>
@stop