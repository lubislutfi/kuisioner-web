@extends('layouts.default')
@section('title', 'Cabang Baru')
@section('content')
<div class="container">
    <div class="row">
        <div id="alert_container" class="alert alert-warning alert-dismissable fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
    </div>
    <form class="form-horizontal">
        {{ csrf_field() }}
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Masukkan data cabang</h3>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="form-group{{ $errors->has('id') ? ' has-error' : '' }}" style="display: none;">
                        <label for="id" class="col-md-3 control-label">ID</label>
                        <div class="col-md-6">
                            <input id="id" type="text" class="form-control" name="id"
                                   value="{{ isset($branches) ? $branches[0]->id : old('id') }}" hidden>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-3 control-label">Nama Cabang</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name"
                                   value="{{ isset($branches) ? $branches[0]->name : old('name') }}"
                                   required
                                   autofocus>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('branchgroup_id') ? ' has-error' : '' }}">
                        <label for="branchgroups_id" class="col-md-3 control-label">Jenis Cabang</label>
                        <div class="col-md-6">
                            <select id="branchgroup_id" class="form-control" name="branchgroup_id"
                                    value="{{ isset($branches) ? $branches[0]->branchgroup_id : old('branchgroup_id') }}">
                                @foreach ($branchgroups as $branchgroup)
                                <option value="{{ $branchgroup->id }}"
                                    @if(isset($branches))
                                        @if( $branchgroup->id == $branches[0]->branchgroup->id)
                                            selected
                                        @endif
                                    @endif >
                                    {{ $branchgroup->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-8">
                        <a href="{{ url('/pengaturan-cabang') }}" class="btn btn-warning pull-right"
                           role="button">Batal</a>
                    </div>

                    <div class="col-sm-1">
                        <a id="btnSubmit" class="btn btn-primary"
                           role="button">Simpan</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {

        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    //initialize variable
    var url;
    var method;
    var type;
    var data;
    console.log($('meta[name="csrf-token"]').attr('content'));
    //hiding alert
    $("#alert_container").hide();

    //defining edit or create
    if ($('#name').val() == '') {
        url = '/kuisioner-ntt-web/public/pengaturan-cabang';
        method = 'POST';
        type = 'create';
    } else {
        url = '/kuisioner-ntt-web/public/pengaturan-cabang/' + $("#id").val();
        method = 'PATCH';
        type = 'edit';
    }

    $("#btnSubmit").click(function () {

        data = {
            'id': $("#id").val(),
            'name': $("#name").val(),
            'branchgroup_id': $("#branchgroup_id").val(),
        };

        $.ajax({
            url: url,
            type: method,
            data: data,
            success: function (result) {
                console.log(url);
                console.log(method);
                console.log(type);

                console.log(result);
                if (result == 'success') {
                    DevExpress.ui.notify('Berhasil Disimpan', 'success', 600);
                    window.location.replace("/kuisioner-ntt-web/public/pengaturan-cabang");
                } else {
                    $("#alert_container").show();

                    // variable keperluan pembuatan alert
                    var node = document.createTextNode(result[key]);
                    var ul = document.createElement("ul");
                    var element = document.getElementById("alert_container");

                    //clear child
                    while (element.hasChildNodes()) {
                        element.removeChild(element.lastChild); //jika memang kau terlahir hanya untukku
                    }

                    //ambil pesan error
                    for (var key in result) {
                        var li = document.createElement("li");
                        var node = document.createTextNode(result[key]);
                        ul.appendChild(li);
                        li.appendChild(node);
                        element.appendChild(ul);
                    }
                }
            }
        });
    });

</script>

@stop