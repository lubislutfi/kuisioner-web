@extends('layouts.default')
@section('title', 'Pengaturan Pertanyaan')
@section('content')
<div class="container" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div id="alert_container" class="alert alert-warning alert-dismissable fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
    </div>
    <form class="form-horizontal">
        {{ csrf_field() }}
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Masukkan pertanyaan</h3>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="form-group{{ $errors->has('id') ? ' has-error' : '' }}" style="display: none;">
                        <label for="id" class="col-md-3 control-label">ID</label>
                        <div class="col-md-6">
                            <input id="id" type="text" class="form-control" name="id"
                                   value="{{ isset($questionlists) ? $questionlists[0]->id : old('id') }}" hidden>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="question" class="col-md-3 control-label">Pertanyaan</label>
                        <div class="col-md-6">
                            <input id="question" type="text" class="form-control" name="question"
                                   value="{{ isset($questionlists) ? $questionlists[0]->question : old('question') }}"
                                   required
                                   autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="customer_service" class="col-md-3 control-label">Ditujukan Untuk</label>
                        <div class="col-md-6">
                            <label class="checkbox-inline">
                                <input id="customer_service" type="checkbox"
                                 @if(isset($questiongroups))
                                    @foreach ($questiongroups as $questiongroup)
                                        @if($questiongroup->usergroup_id == 2)
                                            checked
                                        @endif
                                    @endforeach
                                @endif>Customer Service</label>
                            <label class="checkbox-inline"><input id="teller" type="checkbox"
                                @if(isset($questiongroups))
                                    @foreach ($questiongroups as $questiongroup)
                                        @if($questiongroup->usergroup_id == 3)
                                            checked
                                        @endif
                                    @endforeach
                                @endif>Teller</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-8">
                        <a href="{{ url('/pengaturan-pertanyaan') }}" class="btn btn-warning pull-right"
                           role="button">Batal</a>
                    </div>

                    <div class="col-sm-1">
                        <a id="btnSubmit" class="btn btn-primary"
                           role="button">Simpan</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {

        //inisialisasi token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        //initialize variable
        var url;
        var method;
        var type;
        var data;
        //console.log($('meta[name="csrf-token"]').attr('content'));
        //hiding alert
        $("#alert_container").hide();

        //defining edit or create
        if ($('#question').val() == '') {
            url = '/kuisioner-ntt-web/public/pengaturan-pertanyaan';
            method = 'POST';
            type = 'create';
        } else {
            url = '/kuisioner-ntt-web/public/pengaturan-pertanyaan/' + $("#id").val();
            method = 'PATCH';
            type = 'edit';
        }

        $("#btnSubmit").click(function () {

            data = {
                'id': $("#id").val(),
                'question': $("#question").val(),
                'customer_service': $("#customer_service").is(':checked'),
                'teller': $("#teller").is(':checked'),
            };

            console.log(data)

            $.ajax({
                url: url,
                type: method,
                data: data,
                success: function (result) {

                    console.log(result);
                    if (result == 'success') {
                        DevExpress.ui.notify('Berhasil Disimpan', 'success', 600);
                        window.location.replace("/kuisioner-ntt-web/public/pengaturan-pertanyaan");
                    } else {
                        $("#alert_container").show();

                        // variable keperluan pembuatan alert
                        var node = document.createTextNode(result[key]);
                        var ul = document.createElement("ul");
                        var element = document.getElementById("alert_container");

                        //clear child
                        while (element.hasChildNodes()) {
                            element.removeChild(element.lastChild); //jika memang kau terlahir hanya untukku
                        }

                        //ambil pesan error
                        for (var key in result) {
                            var li = document.createElement("li");
                            var node = document.createTextNode(result[key]);
                            ul.appendChild(li);
                            li.appendChild(node);
                            element.appendChild(ul);
                        }
                    }
                }
            });
        });
    });
</script>

@stop