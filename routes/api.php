<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/getquestion_offline','QuestionListController@apigetquestion');


Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/user','PengaturanUserController@apigetuser');
    Route::get('/getquestion','QuestionListController@apigetquestion');
    Route::post('/storevote', 'VotesController@apistorevote');

    Route::get('/dashboard', function (Request $request) {
        return $request->user();
    });

});



