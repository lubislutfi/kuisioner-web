<?php

Route::post('/json-pivot', 'GetJsonController@getPivotData');
Route::post('/json-pivot-count', 'GetJsonController@getPivotDataCount');
Route::post('/json-pivot-paging', 'GetJsonController@getPivotDataWithPaging');
Route::post('/json-vote', 'GetJsonController@getVoteData');
Route::post('/json-dashboard', 'GetJsonController@getDashboardData');
Route::get('/buatdata', 'GetJsonController@buatdata');
Route::get('/getbuatdata', 'GetJsonController@getProsesBuat');

//keperluan autentikasi
Route::get('/keluar', 'AutentikasiController@logout');
Route::get('/login', 'AutentikasiController@login');
Route::post('/login', 'AutentikasiController@authenticate');

Route::get('/dashboardmobile/{user_id}', 'GetJsonController@getMobileDashboardData');

//halaman admin
//harus terautentikasi terlebih dulu sebelum masuk
Route::group(['middleware' => ['auth']], function () {

    //dashboard
    Route::resource('/', 'DashboardController');

    //menu data
    Route::resource('/data-cabang', 'DataCabangController');
    Route::resource('/data-petugas', 'DataPetugasController');
    Route::resource('/data-vote', 'DataVoteController');

    //menu pengaturan
    Route::resource('/pengaturan-cabang', 'PengaturanCabangController');
    Route::resource('/pengaturan-user', 'PengaturanUserController');
    Route::resource('/pengaturan-pertanyaan', 'QuestionListController');

    //jsondata untuk keperluan penyediaan data ke table
    Route::get('/json-branches', 'GetJsonController@getBranches');
    Route::get('/json-user', 'GetJsonController@getUser');
    Route::get('/json-pertanyaan', 'GetJsonController@getQuestion');

});

