<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $table = 'kuisioner_usergroup';
    protected $hidden = ['id','created_at','updated_at'];
}
