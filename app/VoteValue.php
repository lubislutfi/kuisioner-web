<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoteValue extends Model
{
    protected $table = 'kuisioner_votes_value';
    protected $hidden = ['updated_at'];

    public function votes()
    {
        return $this->belongsTo(Votes::class,'vote_id','id');
    }

    public function questionlist()
    {
        return $this->belongsTo(QuestionList::class,'question_id','id');
    }
}
