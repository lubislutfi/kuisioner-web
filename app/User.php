<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    //login passport biar pakai username, bukan email
    public function findForPassport($username) {
        return $this->where('username', $username)->first();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*protected $fillable = [
        'name','username', 'email', 'password','usergroup_id'
    ];*/

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','created_at','updated_at'
    ];

    public function branches()
    {
        return $this->belongsTo(Branches::class,'branch_id','id');
    }

    public function usergroup()
    {
        return $this->belongsTo(UserGroup::class,'usergroup_id','id');
    }

}
