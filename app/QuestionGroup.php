<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionGroup extends Model
{
    protected $table = 'kuisioner_questiongroup';
    protected $hidden = ['created_at','updated_at'];

    public function usergroup()
    {
        return $this->belongsTo(UserGroup::class,'usergroup_id','id');
    }

    function questionlist()
    {
        return $this->belongsTo(QuestionList::class,'question_id','id');
    }
}
