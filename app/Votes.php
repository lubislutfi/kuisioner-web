<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Votes extends Model
{
    protected $table = 'kuisioner_votes';
    protected $hidden = ['updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function votevalues()
    {
        return $this->belongsTo(VoteValue::class,'vote_id','id');
    }

    public function votevalue()
    {
        return $this->hasMany(VoteValue::class,'vote_id','id');
    }
}
