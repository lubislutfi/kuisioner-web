<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionList extends Model
{
    protected $table = 'kuisioner_questionlist';
    protected $hidden = ['created_at','updated_at'];

    public function questiongroup()
    {
        return $this->hasMany(QuestionGroup::class, 'question_id', 'id')
            ->join('kuisioner_usergroup', 'kuisioner_questiongroup.usergroup_id', '=', 'kuisioner_usergroup.id');
    }

    public function questiongroup_pivot()
    {
        return $this->belongsTo(QuestionGroup::class,'question_id','id');

    }

}
