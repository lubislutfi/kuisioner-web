<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branches extends Model
{
    protected $table = 'kuisioner_branches';
    protected $hidden = ['created_at','updated_at'];

    public function branchgroup()
    {
        return $this->belongsTo(BranchGroup::class, 'branchgroup_id', 'id');
    }

}
