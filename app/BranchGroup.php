<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchGroup extends Model
{
    protected $table ='kuisioner_branchgroup';
    protected $hidden = ['created_at','updated_at'];
}
