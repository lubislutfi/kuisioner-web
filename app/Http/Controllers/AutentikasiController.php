<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;
use Validator;

class AutentikasiController extends Controller
{
    protected $redirectAfterLogout = '/login';

    public function authenticate(Request $request)
    {
        //inisialisasi data login
        $credentials =
            [
                'username' => $request->input('username'),
                'password' => $request->input('password'),
                'usergroup_id' => 1
            ];

        //inisialisasi objek untuk keperluan validator
        $validator = Validator::make($request->all(), [
            'username' => 'required|exists:users,username',
            'password' => 'required',
        ]);

        //mulai validasi, kalau data tidak sesuai format validasi, munculkan error pada form
        if ($validator->fails()) {
            return redirect('login')
                ->withErrors($validator)
                ->withInput();
        } else {
            //kalau berhasil, masuk ke dashboard
            if (Auth::attempt($credentials)) {
                return redirect()->intended('/');
            } else {
                //kalau gagal, muncul pesan error username dan password salah
                $errors = new MessageBag(['username' => ['Username and/or password invalid.']]);
                // if Auth::attempt fails (wrong credentials) create a new message bag instance.
                return redirect('login')->withErrors($errors)->withInput();
            }
        }
    }

    //keperluan buka halaman login
    public function login(Request $request)
    {
        if (Auth::check()) {
            return view('pages.dashboard');
        } else {
            return view('login');
        }
    }

    //logout aplikasi
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect($this->redirectAfterLogout);
    }
}
