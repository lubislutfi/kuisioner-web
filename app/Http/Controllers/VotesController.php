<?php

namespace App\Http\Controllers;

use App\Branches;
use App\User;
use App\Votes;
use App\VoteValue;
use Illuminate\Http\Request;

class VotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //untuk menyimpan data vote mobile
    public function apistorevote(Request $request)
    {
        date_default_timezone_set('Asia/Makassar');

        $user_id = json_decode($request->all()['data'])->user_id;
        $datas = json_decode($request->all()['data'])->vote;

        $users = User::find($user_id);
        $branches = Branches::find($users->branch_id);

        $user_id = $users->id;
        $branch_id = $users->branch_id;
        $usergroup_id = $users->usergroup_id;
        $branchgroup_id = $branches->branchgroup_id;

        $votes = new Votes();
        $votes->user_id = $user_id;

        if($votes->save())
        {
            $vote_id = $votes->id;

            foreach($datas as $data)
            {
                $vote_value = new VoteValue();

                $vote_value->vote_id = $vote_id;
                $vote_value->question_id = $data->question_id;
                $vote_value->value = $data->value;

                $vote_value->user_id = $user_id;
                $vote_value->branch_id = $branch_id;
                $vote_value->usergroup_id = $usergroup_id;
                $vote_value->branchgroup_id = $branchgroup_id;

                $vote_value->save();
            }
            return response()->json([
                'status' => 'success'
            ]);
        }else
        {
            return response()->json([
                'status' => 'failed'
            ]);
        }
    }
}
