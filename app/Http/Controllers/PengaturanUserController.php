<?php

namespace App\Http\Controllers;

use App\Branches;
use App\User;
use App\UserGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;

class PengaturanUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.pengaturan-user');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branchoptions = Branches::select(['kuisioner_branches.id', 'kuisioner_branches.name'])->get();
        $usergroups = UserGroup::select(['kuisioner_usergroup.id', 'kuisioner_usergroup.name'])->get();

        return view('pages.pengaturan-user-buatbaru', ['branchoptions' => $branchoptions, 'usergroups' => $usergroups]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->input('username');
        //inisialisasi objek untuk keperluan validator
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:6|max:35',
            'username' => 'required|min:6|max:25|unique:users,username',
            'email' => 'email|unique:users,email',
            'password' => 'required|min:3|max:20',
            'confirm_password' => 'required|min:3|max:20|same:password',
            'branch_id' => 'required',
            'usergroup_id' => 'required'
        ]);

        //mulai validasi, kalau data tidak sesuai format validasi, munculkan error pada form
        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        } else {
            $users = new User;

            $users->name = $request->name;
            $users->username = $request->username;
            $users->email = $request->email;
            $users->password = Hash::make($request->password);
            $users->branch_id = $request->branch_id;
            $users->usergroup_id = $request->usergroup_id;

            $users->save();

            return 'success';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->with('branches', 'usergroup')->get();
        $branchoptions = Branches::select(['kuisioner_branches.id', 'kuisioner_branches.name'])->get();
        $usergroups = UserGroup::select(['kuisioner_usergroup.id', 'kuisioner_usergroup.name'])->get();
        //return $user;
        return view('pages.pengaturan-user-buatbaru', ['user' => $user, 'branchoptions' => $branchoptions, 'usergroups' => $usergroups]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //inisialisasi objek untuk keperluan validator
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:6|max:35',
            'username' => 'required|min:6|max:25|unique:users,username,'.$request->input('id'),
            'email' => 'email|unique:users,email,'.$request->input('id'),
            'password' => 'min:3|max:20',
            'confirm_password' => 'min:3|max:20|same:password',
            'branch_id' => 'required',
            'usergroup_id' => 'required'
        ]);

        //mulai validasi, kalau data tidak sesuai format validasi, munculkan error pada form
        if ($validator->fails()) {
          /*  return Redirect::to('pengaturan-user/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();*/
            return response()->json($validator->messages(), 200);
        } else {
            $users = User::find($id);

            $request->input('');

            $users->name = $request->input('name');
            $users->username = $request->input('username');
            $users->email = $request->input('email');
            $request->input('password') != '' ? $users->password = Hash::make($request->input('password')):'';
            $users->branch_id =$request->input('branch_id');
            $users->usergroup_id = $request->input('usergroup_id');

            $users->save();

            //return redirect()->intended('/pengaturan-user');
            return 'success';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return view('pages.pengaturan-user');
    }


    //Untuk menampilkan user yang direquest oleh Mobile
    public function apigetuser(Request $request)
    {
        $user = User::select(['users.id', 'users.username','users.email','users.name','branch_id','usergroup_id'])
            ->orderBy($request->input('orderBy', 'id'), $request->input('orderType', 'asc'))
            ->with(['branches' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['usergroup' => function ($query) {
                $query->select('id', 'name');
            }])
            ->where('id','=',$request->user()->id)
            ->get();
        return $user;
    }
}
