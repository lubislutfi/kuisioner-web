<?php

namespace App\Http\Controllers;

use App\QuestionGroup;
use App\QuestionList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuestionListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.pengaturan-pertanyaan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.pengaturan-pertanyaan-buatbaru');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //inisialisasi objek untuk keperluan validator
        $validator = Validator::make($request->all(), [
            'question' => 'required|min:3|max:160|unique:kuisioner_questionlist,question',
            'branchgroup_id' => '',
            'branchgroup_id' => ''
        ]);

        //mulai validasi, kalau data tidak sesuai format validasi, munculkan error pada form
        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        } else {
            $questionlist = new QuestionList();
            $questionlist->question = $request->question;
            $questionlist->save();

            //id untuk disimpan di tabel questiongroup
            $savedId = $questionlist->id;

            if ($request->customer_service == "true") {
                $questiongroup = new QuestionGroup();
                $questiongroup->question_id = $savedId;
                $questiongroup->usergroup_id = 2;
                $questiongroup->save();
            }

            if ($request->teller == "true") {
                $questiongroup = new QuestionGroup();
                $questiongroup->question_id = $savedId;
                $questiongroup->usergroup_id = 3;
                $questiongroup->save();
            }

            return 'success';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $questionlists = QuestionList::where('id', $id)->get();
        $questiongroups = QuestionGroup::select(['kuisioner_questiongroup.question_id', 'kuisioner_questiongroup.usergroup_id'])
            ->where('question_id', $id)
            ->get();

        //return $branches;
        return view('pages.pengaturan-pertanyaan-buatbaru', ['questionlists' => $questionlists, 'questiongroups' => $questiongroups]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //inisialisasi objek untuk keperluan validator
        $validator = Validator::make($request->all(), [
            'question' => 'required|min:3|max:160|unique:kuisioner_questionlist,question,' . $request->id,
            'branchgroup_id' => '',
            'branchgroup_id' => ''
        ]);

        //mulai validasi, kalau data tidak sesuai format validasi, munculkan error pada form
        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        } else {

            $questionlist = QuestionList::find($request->id);
            $questionlist->question = $request->question;
            $questionlist->save();

            if ($request->customer_service == "true") {
                if (!QuestionGroup::where('question_id', '=', $request->id)->where('usergroup_id', '=', 2)->exists()) {
                    $questiongroup = new QuestionGroup();
                    $questiongroup->question_id = $request->id;
                    $questiongroup->usergroup_id = 2;
                    $questiongroup->save();
                }
            }else{
                if (QuestionGroup::where('question_id', '=', $request->id)->where('usergroup_id', '=', 2)->exists()) {
                    $questiongroup = QuestionGroup::where('question_id', '=', $request->id)->where('usergroup_id', '=', 2);
                    $questiongroup->delete();
                }
            }

            if ($request->teller == "true") {
                if (!QuestionGroup::where('question_id', '=', $request->id)->where('usergroup_id', '=', 3)->exists()) {
                    $questiongroup = new QuestionGroup();
                    $questiongroup->question_id = $request->id;
                    $questiongroup->usergroup_id = 3;
                    $questiongroup->save();
                }
            }else{
                if (QuestionGroup::where('question_id', '=', $request->id)->where('usergroup_id', '=', 3)->exists()) {
                    $questiongroup = QuestionGroup::where('question_id', '=', $request->id)->where('usergroup_id', '=', 3);
                    $questiongroup->delete();
                }
            }
            return 'success';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionlist = QuestionList::find($id);
        $questionlist->delete();

        $questiongroup = QuestionGroup::where('question_id', '=', $id);
        $questiongroup->delete();
    }

    //untuk mengambil list pertanyaan dari mobile (berdasarkan user)
    public function apigetquestion(Request $request)
    {
        $question = QuestionGroup::select(['kuisioner_questiongroup.id', 'question_id', 'usergroup_id'])
            ->orderBy($request->input('orderBy', 'id'), $request->input('orderType', 'asc'))
            ->with(['questionlist' => function ($query) {
                $query->select('id','question');
            }])
            ->with(['usergroup' => function ($query) {
                $query->select('id','name');
            }])
            ->where('usergroup_id','=',$request->user()->usergroup_id)
            ->get();
        return $question;
    }
}
