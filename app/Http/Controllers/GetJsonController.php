<?php

namespace App\Http\Controllers;

use App\Branches;
use App\QuestionList;
use App\User;
use App\Votes;
use App\VoteValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GetJsonController extends Controller
{

    function getBranches(Request $request)
    {
        $branches = Branches::select(['kuisioner_branches.name', 'id', 'kuisioner_branches.branchgroup_id'])
            ->orderBy($request->input('orderBy', 'id'), $request->input('orderType', 'asc'))
            ->with(['branchgroup' => function ($query) {
                $query->select('id', 'name');
            }])
            ->paginate(15);
        //where('name','like','%'.$request->filter.'%')

        return $branches;
    }

    function getUser(Request $request)
    {
        $user = User::select(['users.id', 'users.username', 'users.email', 'users.name', 'branch_id', 'usergroup_id'])
            ->orderBy($request->input('orderBy', 'id'), $request->input('orderType', 'asc'))
            ->with(['branches' => function ($query) {
                $query->select('id', 'name');
            }])
            ->with(['usergroup' => function ($query) {
                $query->select('id', 'name');
            }])
            ->paginate(15);

        //where('name','like','%'.$request->filter.'%')

        /* foreach ($offices as $office)
         {
             echo $office->name .' | '. $office->office_type->name.'<br>';
         }*/

        return $user;
    }

    function getQuestion(Request $request)
    {
        $question = QuestionList::select(['kuisioner_questionlist.id', 'kuisioner_questionlist.question'])
            ->orderBy($request->input('orderBy', 'id'), $request->input('orderType', 'asc'))
            ->with(['questiongroup' => function ($query) {
                $query->select('question_id', 'usergroup_id', 'kuisioner_usergroup.name')->orderBy('usergroup_id', 'desc');
            }])
            ->paginate(15);

        //where('name','like','%'.$request->filter.'%')
        /* foreach ($offices as $office)
         {
             echo $office->name .' | '. $office->office_type->name.'<br>';
         }*/

        return $question;
    }

    function getPivotData(Request $request)
    {
        $pivot_data = VoteValue::select(['vote_id', 'question_id', 'value', 'created_at as tanggal'])
            ->with(['votes' => function ($query) {
                $query->select('user_id', 'id')
                    ->with(['user' => function ($query) {
                        $query->select('id', 'usergroup_id', 'branch_id', 'name', 'username')
                            ->with(['usergroup' => function ($query) {
                                $query->select('id', 'name');
                            }])
                            ->with(['branches' => function ($query) {
                                $query->select('id', 'name');
                            }]);
                    }]);
            }])
            ->with(['questionlist' => function ($query) {
                $query->select('id', 'question');
            }])
            ->where(DB::raw('DATE_FORMAT(created_at,"%Y/%c")'), '>=', $request->bulanStart)
            ->where(DB::raw('DATE_FORMAT(created_at,"%Y/%c")'), '<=', $request->bulanEnd)
            ->skip($request->skip)
            ->take($request->take)
            ->get();


        /*     Log::info("-----dataPivot-----");
             Log::info($pivot_data);
             Log::info($request->bulanStart);
             Log::info($request->bulanEnd);
             Log::info("-------------------");*/

        return $pivot_data;
    }

    function getPivotDataCount(Request $request)
    {
        // $request->name;

        $count = VoteValue::select('id')
            ->where(DB::raw('DATE_FORMAT(created_at,"%Y/%c")'), '>=', $request->bulanStart)
            ->where(DB::raw('DATE_FORMAT(created_at,"%Y/%c")'), '<=', $request->bulanEnd)
            ->count();

        /*  Log::info("--------count------");
          Log::info($count);
          Log::info($request->bulanStart);
          Log::info($request->bulanEnd);
          Log::info("-------------------");*/

        return $count;
    }

    function getPivotDataWithPaging(Request $request)
    {
        $pivot_data = VoteValue::select(['vote_id', 'question_id', 'value', 'created_at as tanggal'])
            ->with(['votes' => function ($query) {
                $query->select('user_id', 'id')
                    ->with(['user' => function ($query) {
                        $query->select('id', 'usergroup_id', 'branch_id', 'name', 'username')
                            ->with(['usergroup' => function ($query) {
                                $query->select('id', 'name');
                            }])
                            ->with(['branches' => function ($query) {
                                $query->select('id', 'name');
                            }]);
                    }]);
            }])
            ->with(['questionlist' => function ($query) {
                $query->select('id', 'question');
            }])
            ->skip($request->skip)
            ->take($request->take)
            ->get();

        return $pivot_data;
    }

    function getVoteData(Request $request)
    {
        ini_set('max_execution_time', 60 * 60 * 60 * 3);

        $operatorQuery = "";
        $selectQuery = "";

        $data = DB::table('kuisioner_votes_value as vv');
        $data = $data->select(["vv.vote_id as vote_id", DB::raw("MIN(u.name) as nama"), DB::raw("MIN(br.name) as branch"), DB::raw("MIN(urg.name) as usergroup"), DB::raw("MIN(brg.name) as branchgroup"), DB::raw("SUM(vv.value) as value"), DB::raw("MAX(vv.created_at) as waktu")]);
        $data = $data->leftJoin('users as u', 'vv.user_id', '=', 'u.id');
        $data = $data->leftJoin('kuisioner_usergroup as urg', 'vv.usergroup_id', '=', 'urg.id');
        $data = $data->leftJoin('kuisioner_branches as br', 'vv.branch_id', '=', 'br.id');
        $data = $data->leftJoin('kuisioner_branchgroup as brg', 'vv.branchgroup_id', '=', 'brg.id');

        //filter
        if ($request->has('filter')) {
            if (!is_array($request->input('filter')[0])) {
                //filter cuma satu
                if ($request->input('filter')[1] == "startswith") {
                    $data = $data->where($request->input('filter')[0], 'LIKE', $request->input('filter')[2] . '%');
                } else if ($request->input('filter')[1] == "endswith") {
                    $data = $data->where($request->input('filter')[0], 'LIKE', '%' . $request->input('filter')[2]);
                } else if ($request->input('filter')[1] == "contains") {
                    $data = $data->where($request->input('filter')[0], 'LIKE', '%' . $request->input('filter')[2] . '%');
                } else if ($request->input('filter')[1] == "notcontains") {
                    $data = $data->where($request->input('filter')[0], 'NOT LIKE', '%' . $request->input('filter')[2] . '%');
                } else {
                    $data = $data->where($request->input('filter')[0], $request->input('filter')[1], $request->input('filter')[2]);
                }
            } else {
                //FILTER LEBIH DARI SATU
                //var paymentFilter = [];
                for ($i = 0; $i < count($request->input('filter')); $i++) {

                    if (is_array($request->input('filter')[$i])) {
                        //ISI FILTER

                        if (is_array($request->input('filter')[$i][0])) {
                            //FILTER = TANGGAL DUA WHERE

                            if ($operatorQuery == "" || $operatorQuery == "and") {
                                $data = $data->where(function ($query) use ($request, $i) {
                                    $operatorSubquery = "";

                                    for ($j = 0; $j < count($request->input('filter')[$i]); $j++) {

                                        if (is_array($request->input('filter')[$i][$j])) {
                                            $date = $request->input('filter')[$i][$j][2];

                                            //AMBIL ISI FILTER (SUBQUERY)

                                            if ($operatorSubquery == "" || $operatorSubquery == "and") {
                                                //where
                                                if ($request->input('filter')[$i][$j][0] == "waktu") {

                                                    if ($request->input('filter')[$i][$j][1] == "startswith") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'LIKE', $date . '%');
                                                    } else if ($request->input('filter')[$i][$j][1] == "endswith") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date);
                                                    } else if ($request->input('filter')[$i][$j][1] == "contains") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date . '%');
                                                    } else if ($request->input('filter')[$i][$j][1] == "notcontains") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'NOT LIKE', '%' . $date . '%');
                                                    } else {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], $request->input('filter')[$i][$j][1], $date);
                                                    }
                                                }
                                            } else {
                                                //orwhere
                                                if ($request->input('filter')[$i][$j][0] == "waktu") {
                                                    if ($request->input('filter')[$i][$j][0] == "waktu") {

                                                        if ($request->input('filter')[$i][$j][1] == "startswith") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'LIKE', $date . '%');
                                                        } else if ($request->input('filter')[$i][$j][1] == "endswith") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date);
                                                        } else if ($request->input('filter')[$i][$j][1] == "contains") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date . '%');
                                                        } else if ($request->input('filter')[$i][$j][1] == "notcontains") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'NOT LIKE', '%' . $date . '%');
                                                        } else {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], $request->input('filter')[$i][$j][1], $date);
                                                        }
                                                    }
                                                }
                                            }

                                        } else {
                                            //AMBIL PENGHUBUNG (SUBQUERY)

                                            $operatorSubquery = $request->input('filter')[$i][$j];
                                        }
                                    }
                                    //tambahkan ke main filter
                                    // paymentFilter . push(paymentFilterChild);

                                });
                            } else {
                                $data = $data->orWhere(function ($query) use ($request, $i) {
                                    $operatorSubquery = "";

                                    for ($j = 0; $j < count($request->input('filter')[$i]); $j++) {

                                        if (is_array($request->input('filter')[$i][$j])) {
                                            $date = $request->input('filter')[$i][$j][2];

                                            //AMBIL ISI FILTER (SUBQUERY)

                                            if ($operatorSubquery == "" || $operatorSubquery == "and") {
                                                //where
                                                if ($request->input('filter')[$i][$j][0] == "waktu") {

                                                    if ($request->input('filter')[$i][$j][1] == "startswith") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'LIKE', $date . '%');
                                                    } else if ($request->input('filter')[$i][$j][1] == "endswith") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date);
                                                    } else if ($request->input('filter')[$i][$j][1] == "contains") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date . '%');
                                                    } else if ($request->input('filter')[$i][$j][1] == "notcontains") {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], 'NOT LIKE', '%' . $date . '%');
                                                    } else {
                                                        $query = $query->where($request->input('filter')[$i][$j][0], $request->input('filter')[$i][$j][1], $date);
                                                    }
                                                }
                                            } else {
                                                //orwhere
                                                if ($request->input('filter')[$i][$j][0] == "waktu") {
                                                    if ($request->input('filter')[$i][$j][0] == "waktu") {

                                                        if ($request->input('filter')[$i][$j][1] == "startswith") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'LIKE', $date . '%');
                                                        } else if ($request->input('filter')[$i][$j][1] == "endswith") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date);
                                                        } else if ($request->input('filter')[$i][$j][1] == "contains") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'LIKE', '%' . $date . '%');
                                                        } else if ($request->input('filter')[$i][$j][1] == "notcontains") {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], 'NOT LIKE', '%' . $date . '%');
                                                        } else {
                                                            $query = $query->orWhere($request->input('filter')[$i][$j][0], $request->input('filter')[$i][$j][1], $date);
                                                        }
                                                    }
                                                }
                                            }

                                        } else {
                                            //AMBIL PENGHUBUNG (SUBQUERY)

                                            $operatorSubquery = $request->input('filter')[$i][$j];
                                        }
                                    }
                                });
                            }
                        } else if ($request->input('filter')[$i][0] == "waktu") {
                            $date = $request->input('filter')[$i][2];
                            //filter = Tanggal

                            if ($operatorQuery == "" || $operatorQuery == "and") {
                                //where
                                if ($request->input('filter')[$i][1] == "startswith") {
                                    $data = $data->where($request->input('filter')[$i][0], 'LIKE', $date . '%');
                                } else if ($request->input('filter')[$i][1] == "endswith") {
                                    $data = $data->where($request->input('filter')[$i][0], 'LIKE', '%' . $date);
                                } else if ($request->input('filter')[$i][1] == "contains") {
                                    $data = $data->where($request->input('filter')[$i][0], 'LIKE', '%' . $date . '%');
                                } else if ($request->input('filter')[$i][1] == "notcontains") {
                                    $data = $data->where($request->input('filter')[$i][0], 'NOT LIKE', '%' . $date . '%');
                                } else {
                                    $data = $data->where($request->input('filter')[$i][0], $request->input('filter')[$i][1], $date);
                                }

                            } else {
                                //orwhere
                                if ($request->input('filter')[$i][1] == "startswith") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'LIKE', $date . '%');
                                } else if ($request->input('filter')[$i][1] == "endswith") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'LIKE', '%' . $date);
                                } else if ($request->input('filter')[$i][1] == "contains") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'LIKE', '%' . $date . '%');
                                } else if ($request->input('filter')[$i][1] == "notcontains") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'NOT LIKE', '%' . $date . '%');
                                } else {
                                    $data = $data->orWhere($request->input('filter')[$i][0], $request->input('filter')[$i][1], $date);
                                }
                            }


                        } else {
                            //FILTER = BUKAN TANGGAL

                            if ($operatorQuery == "" || $operatorQuery == "and") {
                                //where
                                if ($request->input('filter')[$i][1] == "startswith") {
                                    $data = $data->where($request->input('filter')[$i][0], 'LIKE', $request->input('filter')[$i][2] . '%');
                                } else if ($request->input('filter')[$i][1] == "endswith") {
                                    $data = $data->where($request->input('filter')[$i][0], 'LIKE', '%' . $request->input('filter')[$i][2]);
                                } else if ($request->input('filter')[$i][1] == "contains") {
                                    $data = $data->where($request->input('filter')[$i][0], 'LIKE', '%' . $request->input('filter')[$i][2] . '%');
                                } else if ($request->input('filter')[$i][1] == "notcontains") {
                                    $data = $data->where($request->input('filter')[$i][0], 'NOT LIKE', '%' . $request->input('filter')[$i][2] . '%');
                                } else {
                                    $data = $data->where($request->input('filter')[$i][0], $request->input('filter')[$i][1], $request->input('filter')[$i][2]);
                                }
                            } else {
                                //orwhere
                                if ($request->input('filter')[$i][1] == "startswith") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'LIKE', $request->input('filter')[$i][2] . '%');
                                } else if ($request->input('filter')[$i][1] == "endswith") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'LIKE', '%' . $request->input('filter')[$i][2]);
                                } else if ($request->input('filter')[$i][1] == "contains") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'LIKE', '%' . $request->input('filter')[$i][2] . '%');
                                } else if ($request->input('filter')[$i][1] == "notcontains") {
                                    $data = $data->orWhere($request->input('filter')[$i][0], 'NOT LIKE', '%' . $request->input('filter')[$i][2] . '%');
                                } else {
                                    $data = $data->orWhere($request->input('filter')[$i][0], $request->input('filter')[$i][1], $request->input('filter')[$i][2]);
                                }
                            }
                        }
                    } else {
                        //AMBIL PENGHUBUNG UTAMA
                        $operatorQuery = $request->input('filter')[$i];
                    }
                }
            }
        }

        if ($request->has('orderBy')) {
            $data = $data->orderBy(DB::raw($request->input('orderBy')), DB::raw($request->input('orderType')));
        } else {
            $data = $data->orderBy('vote_id', 'desc');
        }

        //group by
        $data = $data->groupBy(DB::raw("vote_id"));

        $total = $data;

        $total = $total->get()->count();

        if ($request->has('take')) {
            $data = $data->take($request->input('take'));
        }

        if ($request->has('skip')) {
            $data = $data->skip($request->input('skip'));
        }

        $data = $data->get();


        return response()->json([
            'totalCount' => $total,//$totalcount,
            'data' => $data
        ]);
    }

    function getMobileDashboardData(Request $request, $user_id)
    {
        date_default_timezone_set('Asia/Makassar');

        $chart = Votes::leftJoin(
            DB::raw('(SELECT vote_id as vote_id,SUM(value) as poin FROM kuisioner_votes_value GROUP BY vote_id) b'),
            function ($join) {
                $join->on('id', '=', 'b.vote_id');
            })
            ->select(DB::raw('DATE_FORMAT(created_at,"%d %b") as tanggal,SUM(b.poin) as poin'))
            ->groupBy('tanggal')
            ->limit(7)
            ->orderBy('created_at', 'desc')
            ->where('user_id', '=', $request->user_id)
            ->get();

        return response()->json([
            'waktu' => date('h:i:s', time()),
            'tanggal' => date('d/m/Y', time()),
            'linechart_harian' => $chart,

        ]);
    }

    function getDashboardData(Request $request)
    {
        $linechart_harian = 'linechart_harian';
        $linechart_bulanan = 'linechart_bulanan';
        $best_overall = 'best_overall';
        $best_teller = 'best_teller';
        $best_cs = 'best_cs';
        $barchart_cabang = [];

        date_default_timezone_set('Asia/Makassar');

        $linechart_harian = VoteValue::select(DB::raw('DATE_FORMAT(created_at,"%d/%m/%y") as tanggal, SUM(value) as poin'))
            ->groupBy('tanggal')
            ->latest()
            ->take(15)
            ->orderBy('created_at', 'asc')
            ->get();

        $linechart_bulanan = VoteValue::select(DB::raw('DATE_FORMAT(created_at,"%b %Y") as bulan, SUM(value) as poin'))
            ->groupBy('bulan')
            ->latest()
            ->take(12)
            ->get();

        /*$best_overall = DB::table('users')
            ->leftJoin('kuisioner_usergroup', 'users.usergroup_id', '=', 'kuisioner_usergroup.id')
            ->leftJoin('kuisioner_branches', 'users.branch_id', '=', 'kuisioner_branches.id')
            ->leftJoin(
                DB::raw(
                    '(SELECT a.user_id as user_id, SUM(b.poin) as poin, MIN(a.created_at) as created_at from kuisioner_votes a 
                    LEFT JOIN
                    (SELECT vote_id as vote_id,SUM(value) as poin 
                    FROM kuisioner_votes_value GROUP BY vote_id) b
                    ON a.id = b.vote_id GROUP BY user_id) kuisioner_votes'),
                function ($join) {
                    $join->on('users.id', '=', 'kuisioner_votes.user_id');
                })
            ->select('users.name', 'kuisioner_usergroup.name as usergroup', 'kuisioner_branches.name as branch', 'kuisioner_votes.poin as poin', DB::raw('DATE_FORMAT(kuisioner_votes.created_at,"%Y/%c") as periode'));

        if ($request->has('periode')) {
            Log::info($request->periode);
            $best_overall = $best_overall->where(DB::raw('DATE_FORMAT(kuisioner_votes.created_at,"%Y/%c")'), '=', $request->periode);
        }

        $best_overall = $best_overall->orderBy('poin', 'desc')->take(5)->get();*/

        //---BEST OVERALL----
        $best_overall = DB::table('kuisioner_votes_value as vv')
            ->leftJoin('kuisioner_branches as br', 'vv.branch_id', '=', 'br.id')
            ->leftJoin('users as u', 'vv.user_id', '=', 'u.id')
            ->leftJoin('kuisioner_usergroup as urg', 'vv.usergroup_id', '=', 'urg.id')
            ->select(DB::raw('MIN(u.name) as name,  MIN(br.name) as branch,  MIN(urg.name) as usergroup,  SUM(vv.value) as poin'));
        if ($request->has('periode')) {
            Log::info($request->periode);
            $best_overall = $best_overall->where(DB::raw('DATE_FORMAT(vv.created_at,"%Y/%c")'), '=', $request->periode);
        }
        $best_overall = $best_overall->groupBy('vv.user_id')->orderBy('poin', 'desc')->take(5)->get();
        //---END OF BEST OVERALL---


        /*$best_teller = DB::table('users')
            ->leftJoin('kuisioner_usergroup', 'users.usergroup_id', '=', 'kuisioner_usergroup.id')
            ->leftJoin('kuisioner_branches', 'users.branch_id', '=', 'kuisioner_branches.id')
            ->leftJoin(
                DB::raw(
                    '(SELECT a.user_id as user_id, SUM(b.poin) as poin, MIN(a.created_at) as created_at from kuisioner_votes a 
                    LEFT JOIN
                    (SELECT vote_id as vote_id,SUM(value) as poin 
                    FROM kuisioner_votes_value GROUP BY vote_id) b
                    ON a.id = b.vote_id GROUP BY user_id) kuisioner_votes'),
                function ($join) {
                    $join->on('users.id', '=', 'kuisioner_votes.user_id');
                })
            ->select('users.name', 'kuisioner_usergroup.name as usergroup', 'kuisioner_branches.name as branch', 'kuisioner_votes.poin as poin', DB::raw('DATE_FORMAT(kuisioner_votes.created_at,"%Y/%c") as periode'))
            ->orderBy('poin', 'desc')
            ->where('kuisioner_usergroup.name', 'like', 'teller');

        if ($request->has('periode')) {
            $best_teller = $best_teller->where(DB::raw('DATE_FORMAT(kuisioner_votes.created_at,"%Y/%c")'), '=', $request->periode);
        }

        $best_teller = $best_teller->take(5)->get();*/

        //---BEST TELLER----
        $best_teller = DB::table('kuisioner_votes_value as vv')
            ->leftJoin('kuisioner_branches as br', 'vv.branch_id', '=', 'br.id')
            ->leftJoin('users as u', 'vv.user_id', '=', 'u.id')
            ->leftJoin('kuisioner_usergroup as urg', 'vv.usergroup_id', '=', 'urg.id')
            ->select(DB::raw('MIN(u.name) as name,  MIN(br.name) as branch,  MIN(urg.name) as usergroup,  SUM(vv.value) as poin'))
            ->where('urg.name', 'like', 'teller');

        if ($request->has('periode')) {
            Log::info($request->periode);
            $best_teller = $best_teller->where(DB::raw('DATE_FORMAT(vv.created_at,"%Y/%c")'), '=', $request->periode);
        }
        $best_teller = $best_teller->groupBy('vv.user_id')->orderBy('poin', 'desc')->take(5)->get();
        //---END OF BEST TELLER----


        /* $best_cs = DB::table('users')
             ->leftJoin('kuisioner_usergroup', 'users.usergroup_id', '=', 'kuisioner_usergroup.id')
             ->leftJoin('kuisioner_branches', 'users.branch_id', '=', 'kuisioner_branches.id')
             ->leftJoin(
                 DB::raw(
                     '(SELECT a.user_id as user_id, SUM(b.poin) as poin, MIN(a.created_at) as created_at from kuisioner_votes a
                     LEFT JOIN
                     (SELECT vote_id as vote_id,SUM(value) as poin
                     FROM kuisioner_votes_value GROUP BY vote_id) b
                     ON a.id = b.vote_id GROUP BY user_id) kuisioner_votes'),
                 function ($join) {
                     $join->on('users.id', '=', 'kuisioner_votes.user_id');
                 })
             ->select('users.name', 'kuisioner_usergroup.name as usergroup', 'kuisioner_branches.name as branch', 'kuisioner_votes.poin as poin', DB::raw('DATE_FORMAT(kuisioner_votes.created_at,"%Y/%c") as periode'))
             ->orderBy('poin', 'desc')
             ->where('kuisioner_usergroup.name', 'like', 'Customer Service');

         if ($request->has('periode')) {
             $best_cs = $best_cs->where(DB::raw('DATE_FORMAT(kuisioner_votes.created_at,"%Y/%c")'), '=', $request->periode);
         }

         $best_cs = $best_cs->take(5)->get();*/

        //---BEST CS----
        $best_cs = DB::table('kuisioner_votes_value as vv')
            ->leftJoin('kuisioner_branches as br', 'vv.branch_id', '=', 'br.id')
            ->leftJoin('users as u', 'vv.user_id', '=', 'u.id')
            ->leftJoin('kuisioner_usergroup as urg', 'vv.usergroup_id', '=', 'urg.id')
            ->select(DB::raw('MIN(u.name) as name,  MIN(br.name) as branch,  MIN(urg.name) as usergroup,  SUM(vv.value) as poin'))
            ->where('urg.name', 'like', 'customer service');

        if ($request->has('periode')) {
            Log::info($request->periode);
            $best_cs = $best_cs->where(DB::raw('DATE_FORMAT(vv.created_at,"%Y/%c")'), '=', $request->periode);
        }
        $best_cs = $best_cs->groupBy('vv.user_id')->orderBy('poin', 'desc')->take(5)->get();
        //---END OF CS----


        $json_cabang = Branches::select('name')->get();
        $json_cabang = json_decode($json_cabang, true);
        for ($i = 0; $i < count($json_cabang); $i++) {
            /* $json_item_cs = DB::table('users')
                 ->leftJoin('kuisioner_usergroup', 'users.usergroup_id', '=', 'kuisioner_usergroup.id')
                 ->leftJoin('kuisioner_branches', 'users.branch_id', '=', 'kuisioner_branches.id')
                 ->leftJoin(
                     DB::raw(
                         '(SELECT a.user_id as user_id, SUM(b.poin) as poin from kuisioner_votes a
                     LEFT JOIN
                     (SELECT vote_id as vote_id,SUM(value) as poin
                     FROM kuisioner_votes_value GROUP BY vote_id) b
                     ON a.id = b.vote_id GROUP BY user_id) kuisioner_votes'),
                     function ($join) {
                         $join->on('users.id', '=', 'kuisioner_votes.user_id');
                     })
                 ->select(DB::raw('kuisioner_branches.name as branch,kuisioner_usergroup.name as usergroup, SUM(kuisioner_votes.poin) as poin'))
                 ->orderBy('poin', 'desc')
                 ->where('kuisioner_usergroup.name', 'like', 'Customer Service')
                 ->where('kuisioner_branches.name', 'like', $json_cabang[$i]['name'])
                 ->groupBy(['branch', 'usergroup'])
                 ->get();*/

            $json_item_cs = DB::table('kuisioner_votes_value as vv')
                ->leftJoin('kuisioner_branches as br', 'vv.branch_id', '=', 'br.id')
                ->leftJoin('kuisioner_usergroup as urg', 'vv.usergroup_id', '=', 'urg.id')
                ->select(DB::raw('MIN(br.name) as branch,  MIN(urg.name) as usergroup,  SUM(vv.value) as poin'))
                ->where('urg.name', 'like', 'Customer Service')
                ->where('br.name', 'like', $json_cabang[$i]['name']);

            if ($request->has('periode')) {
                Log::info($request->periode);
                $json_item_cs = $json_item_cs->where(DB::raw('DATE_FORMAT(vv.created_at,"%Y/%c")'), '=', $request->periode);
            }
            $json_item_cs = $json_item_cs->groupBy(['vv.branchgroup_id','vv.usergroup_id'])->orderBy('poin', 'desc')->get();

           /* $json_item_teller = DB::table('users')
                ->leftJoin('kuisioner_usergroup', 'users.usergroup_id', '=', 'kuisioner_usergroup.id')
                ->leftJoin('kuisioner_branches', 'users.branch_id', '=', 'kuisioner_branches.id')
                ->leftJoin(
                    DB::raw(
                        '(SELECT a.user_id as user_id, SUM(b.poin) as poin from kuisioner_votes a 
                    LEFT JOIN
                    (SELECT vote_id as vote_id,SUM(value) as poin 
                    FROM kuisioner_votes_value GROUP BY vote_id) b
                    ON a.id = b.vote_id GROUP BY user_id) kuisioner_votes'),
                    function ($join) {
                        $join->on('users.id', '=', 'kuisioner_votes.user_id');
                    })
                ->select(DB::raw('kuisioner_branches.name as branch,kuisioner_usergroup.name as usergroup, SUM(kuisioner_votes.poin) as poin'))
                ->orderBy('poin', 'desc')
                ->where('kuisioner_usergroup.name', 'like', 'Teller')
                ->where('kuisioner_branches.name', 'like', $json_cabang[$i]['name'])
                ->groupBy(['branch', 'usergroup'])
                ->get();*/

            $json_item_teller = DB::table('kuisioner_votes_value as vv')
                ->leftJoin('kuisioner_branches as br', 'vv.branch_id', '=', 'br.id')
                ->leftJoin('kuisioner_usergroup as urg', 'vv.usergroup_id', '=', 'urg.id')
                ->select(DB::raw('MIN(br.name) as branch,  MIN(urg.name) as usergroup,  SUM(vv.value) as poin'))
                ->where('urg.name', 'like', 'Teller')
                ->where('br.name', 'like', $json_cabang[$i]['name']);

            if ($request->has('periode')) {
                Log::info($request->periode);
                $json_item_teller = $json_item_teller->where(DB::raw('DATE_FORMAT(vv.created_at,"%Y/%c")'), '=', $request->periode);
            }
            $json_item_teller = $json_item_teller->groupBy(['vv.branchgroup_id','vv.usergroup_id'])->orderBy('poin', 'desc')->get();


            $poin_cs = (count($json_item_cs) != 0) ? $json_item_cs[0]->poin : 0;
            $poin_teller = (count($json_item_teller) != 0) ? $json_item_teller[0]->poin : 0;

            array_push($barchart_cabang, array(
                'branch' => $json_cabang[$i]['name'],
                'cs' => $poin_cs,
                'teller' => $poin_teller
            ));
        }

        return response()->json([
            'waktu' => date('h:i:s', time()),
            'tanggal' => date('d', time()),
            'bulan' => $this->bulan(date('m', time())),
            'tahun' => date('Y', time()),
            'linechart_harian' => $linechart_harian,
            'linechart_bulanan' => $linechart_bulanan,
            'best_overall' => $best_overall,
            'best_teller' => $best_teller,
            'best_cs' => $best_cs,
            'barchart_cabang' => $barchart_cabang,
        ]);
    }

    function bulan($bln)
    {
        $bulan = $bln;
        Switch ($bulan) {
            case 1 :
                $bulan = "Januari";
                Break;
            case 2 :
                $bulan = "Februari";
                Break;
            case 3 :
                $bulan = "Maret";
                Break;
            case 4 :
                $bulan = "April";
                Break;
            case 5 :
                $bulan = "Mei";
                Break;
            case 6 :
                $bulan = "Juni";
                Break;
            case 7 :
                $bulan = "Juli";
                Break;
            case 8 :
                $bulan = "Agustus";
                Break;
            case 9 :
                $bulan = "September";
                Break;
            case 10 :
                $bulan = "Oktober";
                Break;
            case 11 :
                $bulan = "November";
                Break;
            case 12 :
                $bulan = "Desember";
                Break;
        }
        return $bulan;
    }

    function getProsesBuat(Request $request)
    {
        $jumVotes = Votes::select(DB::raw('user_id,count(id) as jumVotes,DATE(created_at) as tanggal'))
            ->groupBy(['user_id', 'tanggal'])
            ->orderBy(DB::raw('tanggal,user_id'), 'asc')
            ->get();
        $jumVoteValue = VoteValue::select(DB::raw('count(vote_id) as jumVoteValue'))
            ->get();

        return response()->json([
            'waktu' => date('h:i:s', time()),
            'votes' => $jumVotes,
            'votesvalue' => $jumVoteValue
        ]);
    }

    function buatData(Request $request)
    {
        ini_set('max_execution_time', 60 * 60 * 60 * 3);

        $jumlahHari = 160;

        $startDate = new \DateTime('2017-01-01 00:00:00');
        $tanggal = $startDate->format('Y-m-d H:i:s');

        //date("Y-M-d H:i:s", mktime(1,2,3,12,3,1975));

        $iduser = array("1", "3", "6", "7", "109", "110", "111", "112", "113", "114", "115", "116", "117", "118", "119", "120", "121", "122", "123", "124", "125", "126", "127", "128", "129", "130", "131", "132", "133", "134", "135", "136", "137", "138", "139", "140", "141", "142", "143", "144", "145", "146", "147", "148", "149", "150", "151", "152", "153", "154", "155", "156", "157", "158", "159", "160", "161", "162", "163", "164", "165", "166", "167", "168", "169", "170", "171", "172", "173", "174");

        for ($m = 0; $m < $jumlahHari; $m++) {
            for ($l = 0; $l < count($iduser); $l++) {
                for ($i = 0; $i < rand(30, 500); $i++) {

                    $users = User::find($iduser[$l]);
                    $branches = Branches::find($users->branch_id);

                    $user_id = $users->id;
                    $branch_id = $users->branch_id;
                    $usergroup_id = $users->usergroup_id;
                    $branchgroup_id = $branches->branchgroup_id;

                    $votes = new Votes();
                    $votes->user_id = $user_id;
                    $votes->created_at = $tanggal;
                    $votes->updated_at = $tanggal;

                    if ($votes->save()) {
                        $vote_id = $votes->id;

                        for ($j = 1; $j <= 3; $j++) {
                            $vote_value = new VoteValue();

                            $vote_value->vote_id = $vote_id;
                            $vote_value->question_id = $j;
                            $vote_value->user_id = $user_id;
                            $vote_value->branch_id = $branch_id;
                            $vote_value->usergroup_id = $usergroup_id;
                            $vote_value->branchgroup_id = $branchgroup_id;

                            $vote_value->value = rand(1, 3);
                            $vote_value->created_at = $tanggal;
                            $vote_value->updated_at = $tanggal;
                            $vote_value->save();
                        }
                    }
                }

                $startDate->modify('+1 day');
                $tanggal = $startDate->format('Y-m-d H:i:s');
            }

            return response()->json([
                'status' => 'success'
            ]);
        }

    }
}
