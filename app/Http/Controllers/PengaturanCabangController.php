<?php

namespace App\Http\Controllers;

use App\Branches;
use App\BranchGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PengaturanCabangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view('pages.pengaturan-cabang');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branchgroups = BranchGroup::select(['kuisioner_branchgroup.id', 'kuisioner_branchgroup.name'])->get();
        return view('pages.pengaturan-cabang-buatbaru', ['branchgroups' => $branchgroups]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //inisialisasi objek untuk keperluan validator
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:35|unique:kuisioner_branches,name',
            'branchgroup_id' => 'required'
        ]);

        //mulai validasi, kalau data tidak sesuai format validasi, munculkan error pada form
        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        } else {
            $branches = new Branches();

            $branches->name = $request->name;
            $branches->branchgroup_id = $request->branchgroup_id;

            $branches->save();

            return 'success';
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branches = Branches::where('id', $id)->get();
        $branchgroups = BranchGroup::select(['kuisioner_branchgroup.id', 'kuisioner_branchgroup.name'])->get();

        return view('pages.pengaturan-cabang-buatbaru', ['branches' => $branches, 'branchgroups' => $branchgroups]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //inisialisasi objek untuk keperluan validator
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:35|unique:kuisioner_branches,name,' . $request->input('id'),
            'branchgroup_id' => 'required'
        ]);

        //mulai validasi, kalau data tidak sesuai format validasi, munculkan error pada form
        if ($validator->fails()) {
            return response()->json($validator->messages(), 200);
        } else {
            $branches = Branches::find($request->id);

            $branches->name = $request->input('name');
            $branches->branchgroup_id = $request->input('branchgroup_id');

            $branches->save();

            //return redirect()->intended('/pengaturan-user');
            return 'success';
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branches = Branches::find($id);
        $branches->delete();
        return view('pages.pengaturan-cabang');
    }
}
